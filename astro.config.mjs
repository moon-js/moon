import { defineConfig } from 'astro/config';
import react from '@astrojs/react';
import tailwind from "@astrojs/tailwind";

export default defineConfig({
  root: '.',     // Where to resolve all URLs relative to. Useful if you have a monorepo project.
  srcDir: './src', // Path to Astro components, pages, and data
  // publicDir: './static',
  // outDir: './public',


  outDir: './public',
  publicDir: './builds/moon-js/moon',

  // Generate sitemap (set to "false" to disable)
  sitemap: true,
  site: 'https://moon-js.gitlab.io',
  base: '/moon',


  //buildOptions: {
  //   // Your public domain, e.g.: https://my-site.dev/. Used to generate sitemaps and canonical URLs.
  //   //site: 'http://example.com',
  //   site: 'https://moon-js.gitlab.io',
  //   base: '/moon',
  //
  //},


  integrations: [react(), tailwind()]
});