##  By inviting Moon.js to your discord server you agree that you have read, understood, and accepted these terms. You are also responsible for informing the members in your discord server about these terms. If you do not agree with any of these terms, you are prohibited from using or adding any version of Moon.js to your server.

### Disclaimer
> You are strictly prohibited to use Moon.js against the ToS of discord or for illegal purposes. We are doing our best to prevent these activities, while trying to provide the best user experience as possible. If you find people or communities using Moon.js against the ToS of discord or even for illegal activities, please send us an email to \<email\>

### Proprietary Rights
> We (Moon.js) own and retain all rights for public available data (including but not limited to templates). We grant you the permission to use this available data for your own needs, but strictly disallow any commercial use. You therefore shall not sell, license or otherwise commercialize the data except if the permission was expressly granted to you.

### Availability
> -    Moon.js is provided as-is. There are no guarantees that it will be available in the future, and its purpose or availability may be changed at any time.
> -    User related data including backups may be deleted at any time.
> -    User related data including backups is non-transferable between discord accounts.
> -    Any premium features are not guaranteed. They may change or be revoked at any time.
> -    Access to all or specific features of Moon.js may be revoked, for all or a specific user, at any time.

### Refund Policy
> Refunds for payments may be issued when the user requests it and one of the following requirements is met:
> - It is the first payment and the user requests a refund within the 24 hours
> - It is a monthly recurring payment and the user requests a refund in a three day period
> - It is an annual recurring payment and the user requests a refund in a seven day period
> - A major premium feature was removed and the user bought the tier specifically because of that feature
>
> We still require a comprehensible explanation of the situation and never guarantee a refund. Partial refunds might be issued for annual payments under special circumstances. Refunds are never issued if > you have broken our Terms of Service and therefore lost access to certain or all features.