import { useState } from "react";
import { motion } from "framer-motion";

function LinkWord(iIndex, sHref) {
  var jqSentence = $('div#sentence');
  var aSentence  = jqSentence.html().split(' ');

  aSentence[iIndex] = '<a href="' + sHref + '">' + aSentence[iIndex] + '</a>';
  jqSentence.html(aSentence.join(' '));
  return jqSentence;
}

const FAQData = [
  {
    question: "Mauris ornare vehicula metus, eu?",
    answer:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in nunc interdum est commodo egestas. Curabitur mauris lacus, sagittis interdum dignissim ac, tempor dignissim felis. Nulla sit amet lobortis diam, eget lobortis nunc. Vestibulum porttitor risus eget tellus lacinia commodo. Etiam ornare fermentum aliquet. Nam elementum, nibh id congue iaculis, orci diam convallis justo, sed finibus ex sem in dui. Nulla facilisi. "
  },
  {
    question: "Aliquam sit amet scelerisque elit.",
    answer:
      "Aliquam dignissim convallis ex, sed euismod felis eleifend eu. Nulla mollis vulputate odio et consectetur. Nulla feugiat egestas erat, sed luctus tortor consectetur sollicitudin. Etiam at urna eget purus ultricies bibendum. Integer venenatis ultricies elementum. Sed eget neque non nulla facilisis venenatis. Aenean quam dolor, varius eu vehicula sit amet, scelerisque non enim. Nam tincidunt lectus fringilla venenatis ornare. Cras dictum arcu id cursus vestibulum. Vivamus eget sem vel enim varius congue. Phasellus quam magna, fermentum nec luctus sed, fermentum eget velit. Donec rhoncus odio lacus, at malesuada velit sollicitudin a. Aliquam fringilla pretium libero, vel finibus ipsum rutrum in. Nullam et justo non purus sodales varius. Phasellus dapibus vitae ligula sed viverra."
  },
  {
    question: "Aliquam vulputate lacus eu tempus vestibulum, nam. ?",
    answer:
      "Vivamus turpis erat, pharetra eget pretium vitae, fringilla lacinia nisi. Praesent turpis nisl, ullamcorper sit amet iaculis ut, finibus tempor nisi. Nunc sagittis tortor mi, et convallis turpis rutrum ut. Vivamus euismod lobortis cursus. Aenean dapibus odio sed porttitor dignissim. Donec a neque nisi. Vestibulum fermentum sit amet tellus imperdiet eleifend. Praesent ac odio at erat pretium accumsan ut at velit. Proin et dui blandit, gravida tellus vel, malesuada augue. Fusce egestas purus vel neque ultrices, ac egestas eros semper. Nullam in venenatis lacus. Aenean fringilla turpis et nisi ultrices, eu ultrices tortor scelerisque. Sed mi sem, faucibus vel ante vel, pulvinar molestie dolor. Vestibulum a tristique neque. Nullam nulla neque, blandit vel molestie et, semper non felis. Sed a dignissim orci, posuere ornare ante. "
  },
  {
    question: "Integer mi metus, vehicula et dictum vitae, scelerisque sit amet sem?",
    answer:
      "Vestibulum imperdiet ligula vulputate orci tristique tincidunt. Praesent in quam et lectus pretium sagittis malesuada in risus. Curabitur sit amet neque magna. Aenean condimentum a purus eu malesuada. Proin metus ligula, dignissim nec magna quis, vehicula elementum nunc. In tempor eros vitae facilisis varius. Donec cursus malesuada nulla, a pharetra tellus auctor et. Ut luctus mauris odio, a vestibulum libero"
  },
];

export const FAQ = () => (
  <section className="relative pt-16 pb-16 bg-blueGray-50 overflow-hidden">
    <div className="absolute -top-10" id="FAQ" />
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
      transition={{ duration: 0.5, delay: 0.2 }}
    >
      <div className="relative z-10 container px-2 sm:px-8 lg:px-4 mx-auto w-11/12 sm:w-full">
        <div className="md:max-w-4xl mx-auto">
          <p className="mb-7 custom-block-subtitle text-center">
            Have any questions?
          </p>
          <h2 className="mb-16 custom-block-big-title text-center">
            Frequently Asked Questions
          </h2>
          <div className="mb-11 flex flex-wrap -m-1">
            {FAQData.map((item, index) => (
              <div className="w-full p-1">
                <FAQBox
                  title={item.question}
                  content={item.answer}
                  key={`${item.question}-${item.answer}`}
                  defaultOpen={index === 0}
                  last={index === FAQData.length-1}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </motion.div>
  </section>
);

const FAQBox = ({ defaultOpen, title, content, last }) => {
  const [isOpen, setIsOpen] = useState(defaultOpen);

  return (
    <div
      className="pt-2 sm:pt-6 pb-2 px-3 sm:px-8  rounded-3xl bg-customDarkBg3 custom-border-gray-darker mb-4 relative hover:bg-customDarkBg3Hover cursor-pointer"
      onClick={() => setIsOpen(!isOpen)}
    >
      <div className="flex flex-col p-2  justify-center items-start">
        <h3 className=" custom-content-title pt-3 sm:pt-0 pr-8 sm:pr-0">
          {title}
        </h3>
        <p
          className={`text-customGrayText pt-4 transition-all duration-300 overflow-hidden ${
            isOpen ? "max-h-96" : "max-h-0"
          }`}
        >
          {content}{last ? <a href="https://youtu.be/dQw4w9WgXcQ?si=XtyLZVWjuN4vbUzR" target="_blank" className="text-gray-100 ml-1.5"><u>tincidunt ut.  </u></a> : ''}

        </p>
      </div>
      <div className="absolute top-6 right-4 sm:top-8 sm:right-8">
        <svg
          width="28px"
          height="30px"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          // ==================================
          className={`transition-all duration-500  ${
            isOpen ? "rotate-[180deg]" : "rotate-[270deg]"
          }`}
        >
          <path
            d="M4.16732 12.5L10.0007 6.66667L15.834 12.5"
            stroke="#4F46E5"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          ></path>
        </svg>
      </div>
    </div>
  );
};
